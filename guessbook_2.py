from turtle import*

setup(600,650)
penup()
goto(-200,250)
pendown()


write("welcome to the party!", font=("Arial",28,"normal"))
add_new = textinput("Name","New Guess? (View, Yes, No)")

names=[]
while not (add_new == "No"):    
    if (add_new == "View"):  
        names.sort()        
        for i in range(len(names)):
            write(names[i])   
    elif (add_new == "Yes"):        
        new_name=textinput("Name","enter your name:")        
        names.append(new_name)    
    add_new=textinput("Continue?","What would you like to do? ")

done()